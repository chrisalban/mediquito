<?php

namespace App\Http\Controllers\Appointment;

use App\Events\AppointmentCancelled;
use App\Events\AppointmentRescheduled;
use App\Events\AppointmentScheduled;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Repositories\AppointmentRepository;
use App\Http\Requests\RescheduleAppointmentRequest;
use App\Http\Resources\AppointmentListResource;
use App\Http\Requests\StoreAppointmentRequest;
use App\Http\Requests\UpdateDescriptionAppointmentRequest;
use App\Http\Resources\AppointmentShowResource;
use App\Models\Appointment;
use App\Services\AppointmentService;

use PDF;

class AppointmentController extends Controller
{
    public function __construct(
        protected AppointmentService $service, 
        protected AppointmentRepository $appointment
    ) {
        $this->middleware('auth:sanctum');
        $this->authorizeResource(Appointment::class, 'appointment');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('doctor')) {
            $appointments = $request->filled('doctor') ? $this->appointment->appointmentsFromDoctor($request->doctor)->get() : [];
        } else if($request->has('time')) {
            $appointments = $this->appointment->appointmentsFromCurrentUser($request->filter_by, $request->time)->get();
        } else {
            $appointments = $this->appointment->appointmentsFromCurrentUser($request->filter_by)->get();
        }
            
        return AppointmentListResource::collection($appointments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAppointmentRequest $request)
    {
        $this->service->scheduleAppointment($request->only([ 'start_time', 'end_time' ]), $request->patient, $request->speciality, $request->doctor, $request->previous);

        AppointmentScheduled::dispatch($this->service->appointment);

        return response()->json([ 'success' => 'appointment scheduled' ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function show(Appointment $appointment)
    {
        return new AppointmentShowResource($this->appointment->showAppointment($appointment));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function reschedule(RescheduleAppointmentRequest $request, Appointment $appointment)
    {
        $this->authorize('update', $appointment);

        $this->service->rescheduleAppointment($appointment, $request->only([ 'start_time', 'end_time' ]), $request->patient, $request->speciality, $request->doctor, $request->previous);
         
        AppointmentRescheduled::dispatch($this->service->appointment);

        return response()->json([ 'success' => 'appointment rescheduled' ]);
    }

    public function updateDescription(UpdateDescriptionAppointmentRequest $request, Appointment $appointment)
    {
        $this->authorize('update', $appointment);

        $this->service->updateDescription($appointment, $request->description);

        return response()->json([ 'success' => 'description update' ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Appointment $appointment)
    {
        $this->service->cancellAppointment($appointment);

        AppointmentCancelled::dispatch($this->service->appointment);

        return response()->json([ 'success' => 'appointment cancelled' ]);
    }

    public function close(Appointment $appointment)
    {
        $this->service->closeAppointment($appointment);

        return response()->json([ 'success' => 'appointment closed' ]);
    }

    public function downloadPrescription(Appointment $appointment)
    {
        $this->authorize('pdf', $appointment);

        $appointment->load('patient', 'doctor', 'prescriptions');

        $patient        = $appointment->patient;
        $doctor         = $appointment->doctor;
        $prescriptions  = $appointment->prescriptions;

        $pdf = PDF::loadView('exports.pdf.prescription', compact('patient', 'doctor', 'prescriptions'));
        return $pdf->download('prescripción.pdf');
    }
}
