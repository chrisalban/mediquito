<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\EmailVerificationRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Traits\AuthTrait;

class LoginController extends Controller
{
    use AuthTrait;

    public function authenticate(Request $request)
    {
        return $this->login($request->only('username', 'password'), $request, $request->remember)
            ?? response()->json(['error' => 'Usuarios o contraseña incorrectos'], 401);
    }

    public function deauthenticate(Request $request)
    {
        $this->logout($request);

        return redirect('/login');
    }

    public function verifyEmail(EmailVerificationRequest $request)
    {
        $request->fulfill();

        return redirect('/home');
    }

    public function resendVerificationEmail(Request $request)
    {
        $request->user()->sendEmailVerificationNotification();

        return back();
    }
}
