<?php

namespace App\Http\Controllers\Prescription;

use App\Http\Controllers\Controller;
use App\Http\Repositories\PrescriptionRepository;
use App\Http\Requests\StorePrescriptionRequest;
use App\Http\Requests\UpdatePrescriptionRequest;
use App\Http\Resources\PrescriptionListResource;
use App\Services\PrescriptionService;
use App\Models\Prescription;
use Illuminate\Http\Request;

class PrescriptionController extends Controller
{
    public function __construct(
        protected PrescriptionService $service
    ) {
        $this->middleware('auth:sanctum');
        $this->authorizeResource(Prescription::class, 'prescription');
    }

    public function index(Request $request, PrescriptionRepository $prescription)
    {
        return PrescriptionListResource::collection($prescription->getPrescriptions($request->filter_by));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePrescriptionRequest $request)
    {
        $this->service->storePrescription($request->validated(), $request->appointment);
        return response()->json([
           'success' => 'prescription created' 
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Prescription  $prescription
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePrescriptionRequest $request, Prescription $prescription)
    {
        $this->service->updatePrescription($request->validated(), $prescription);
        return response()->json([
            'success' => 'prescription updated'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Prescription  $prescription
     * @return \Illuminate\Http\Response
     */
    public function destroy(Prescription $prescription)
    {
        $this->service->deletePrescription($prescription);
        return response()->json([
            'success' => 'prescription deleted'
        ]);
    }
}
