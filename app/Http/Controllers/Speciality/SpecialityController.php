<?php

namespace App\Http\Controllers\Speciality;

use App\Http\Controllers\Controller;
use App\Models\Speciality;
use App\Http\Requests\StoreSpecialityRequest;
use App\Http\Requests\UpdateSpecialityRequest;
use Illuminate\Http\Request;
use App\Http\Resources\SpecialityResource;
use App\Services\SpecialityService;

class SpecialityController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:sanctum');
        $this->authorizeResource(Speciality::class, 'speciality');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return SpecialityResource::collection(Speciality::filterByName($request->filter_by)->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSpecialityRequest $request)
    {
        Speciality::create($request->validated());
        return response()->json([ 'success' => 'speciality stored' ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Speciality  $speciality
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSpecialityRequest $request, Speciality $speciality)
    {
        $speciality->update($request->validated());

        return response()->json([ 'success' => 'speciality updated' ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Speciality  $speciality
     * @return \Illuminate\Http\Response
     */
    public function destroy(Speciality $speciality, SpecialityService $service)
    {
        $service->deleteSpecialityiIfDoesntHaveUsers($speciality);
        return response()->json([ 'success' => 'speciality deleted' ]);
    }
}
