<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserListResource;
use App\Http\Resources\UserShowResource;
use App\Http\Repositories\UserRepository;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Services\UserService;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Notifications\UserCreated;
use App\Notifications\UserUpdated;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function __construct(
        protected UserService $service,
    ) {
        $this->middleware('auth:sanctum');
        $this->authorizeResource(User::class, 'user');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, UserRepository $user)
    {
        return UserListResource::collection( match(true) 
        {
            $request->has('speciality') => $user->getDoctorUsers((int) $request->speciality),
            default => $user->getHospitalUsersWithSpecialityAndRole($request->filter_by)
        });
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        $validated = $request->validated();

        DB::transaction(fn() => $this->service
                                    ->createAddress($request->address)
                                    ->makeUser($validated)
                                    ->assignRole($validated['role'])
                                    ->attachSpecialities($validated['specialities']));

        $this->service->user->notify(new UserCreated($request->username, $request->password));

        return response()->json([ 'success' => 'user created' ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return new UserShowResource($user->load('role', 'specialities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $validated = $request->validated();

        DB::transaction(fn() => $this->service
                                    ->updateAddress($request->address, $user->address)
                                    ->updateUser($validated, $user)
                                    ->assignRole($validated['role'])
                                    ->syncSpecialities($validated['specialities']));

        if($request->filled('password')) {
            $this->service->user->notify(new UserUpdated($request->username, $request->password)); 
        }

        return response()->json([ 'success' => 'user updated' ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return response()->json([ 'success' => 'user deleted' ]);
    }

    public function restore(Request $request)
    {
        $this->authorize('restore', User::class);

        $this->service->restoreUser($request->identification_card);

        return response()->json([ 'success' => 'user restored' ]);
    }
}
