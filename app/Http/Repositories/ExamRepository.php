<?php
namespace App\Http\Repositories;

use App\Models\Exam;
use Illuminate\Support\Facades\Auth;

/*
 * Repositorio para los exámenes
 */
class ExamRepository
{
    public function getPendingExams(String $filter_by = null)
    {
        return Exam::where('status', Exam::PROCESSING_STATUS)
            ->latest()
            ->filterExamByNameOrDoctorOrPatient($filter_by)
            ->with('files', 'appointment.doctor', 'appointment.patient')
            ->get();
    }

    public function getAllExams(String $filter_by = null)
    {
        $exams = Exam::latest()
                     ->filterExamByNameOrDoctorOrPatient($filter_by);

        $user = Auth::user();

        // Si el usuario no es administrador consultar solo los que le pertenecen
        if (!$user->is_admin) {
            if ($user->is_client) {
                // Consultar las citas de un cliente
                $ids = $user->patients->pluck('id')->all();
                $exams->whereHas('appointment.patient', fn ($patient) => $patient->whereIn('id', $ids));
            } else {
                // Consultar las citas de un doctor
                $exams->whereHas('appointment.doctor', fn ($doctor) => $doctor->where('id', $user->id));
            }
        }

        $exams->with('files', 'appointment.doctor', 'appointment.patient');

        return $exams->get();
    }
}
