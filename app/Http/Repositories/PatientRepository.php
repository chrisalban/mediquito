<?php
namespace App\Http\Repositories;

use Illuminate\Support\Facades\Auth;

use App\Models\Patient;

/**
 * Repositorio para la clase de pacientes
 */
class PatientRepository
{
    /*
    * Metodo que consulta los pacientes de un cliente o todos para un administrador
    */
    public function getPatientsFromCurrentUser(String $filter_by = null)
    {
        $user = Auth::user();
        
        $patients = $user->is_client ? $user->patients() : Patient::select('id', 'name', 'last_name');

        $patients->filterByNameAndLastname($filter_by);

        return $patients;
    }
}
