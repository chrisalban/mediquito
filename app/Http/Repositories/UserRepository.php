<?php
namespace App\Http\Repositories;

use App\Models\User;

/*
* Repositorio para la clase usuarios
*/
class UserRepository
{
    /*
    * Metodo que consulta el listado del personal del hospital
    * excluyendo clientes y carga las relaciones especialidad y rol
    */
    public function getHospitalUsersWithSpecialityAndRole(String $filter_by = null)
    {
        return User::whereDoesntHaveRole('client')
            ->filterByNameAndLastname($filter_by)
            ->with('specialities', 'role')
            ->get();
    }

    /*
    * Método que consulta los doctores de una especialidad
    */
    public function getDoctorUsers(int $speciality)
    {
        if (!isset($speciality)) {
            return [];
        }

        return User::whereHasRole('doctor')
            ->whereHas('specialities', fn ($specialities) => $specialities->whereKey($speciality))
            ->get();
    }
}
