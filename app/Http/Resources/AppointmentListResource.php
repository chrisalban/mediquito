<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AppointmentListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'start_time'    => $this->start_time,
            'end_time'      => $this->end_time,
            'description'   => $this->description,
            'is_cancelled'  => $this->is_cancelled,
            'closed_at'     => $this->closed_at,
            'doctor'        => UserListResource::make($this->whenLoaded('doctor')),
            'patient'       => PatientListResource::make($this->whenLoaded('patient')),
        ];
    }
}
