<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PrescriptionListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'drug'       => $this->drug,
            'medication' => $this->medication,
            'doctor'     => UserListResource::make($this->appointment->doctor),
            'patient'    => PatientListResource::make($this->appointment->patient),
        ];
    }
}
