<?php

namespace App\Listeners;

use App\Events\AppointmentRescheduled;
use App\Notifications\AppointmentRescheduled as NotificationsAppointmentRescheduled;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

class AppointmentRescheduleNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AppointmentRescheduled  $event
     * @return void
     */
    public function handle(AppointmentRescheduled $event)
    {
        Notification::send([
            $event->appointment->doctor,
            $event->appointment->patient
        ], new NotificationsAppointmentRescheduled($event->appointment));
    }
}
