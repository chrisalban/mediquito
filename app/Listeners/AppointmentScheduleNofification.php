<?php

namespace App\Listeners;

use App\Events\AppointmentScheduled;
use App\Notifications\AppointmentScheduled as NotificationsAppointmentScheduled;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

class AppointmentScheduleNofification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AppointmentScheduled  $event
     * @return void
     */
    public function handle(AppointmentScheduled $event)
    {
        Notification::send([
            $event->appointment->doctor,
            $event->appointment->patient
        ], new NotificationsAppointmentScheduled($event->appointment));
    }
}
