<?php

namespace App\Models;

use App\Traits\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    use HasFactory, Filterable;

    const PROCESSING_STATUS = 'PROCESSING';
    const READY_STATUS = 'READY';

    protected $fillable = [
        'name', 'description', 'status'
    ];

    public function appointment()
    {
        return $this->belongsTo(Appointment::class);
    }

    public function files()
    {
        return $this->hasMany(File::class);
    }
}
