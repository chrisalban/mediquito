<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

use App\Traits\Filterable;

class Patient extends Model
{
    use HasFactory, Filterable, Notifiable;

    const FEMALE_GENDER = 'F';
    const MALE_GENDER = 'M';
    const HEADLINE_RELATIONSHIP = 'HEADLINE';
    const SPOUSE_RELATIONSHIP = 'SPOUSE';
    const SON_RELATIONSHIP = 'SON';
    const DAUGHTER_RELATIONSHIP = 'DAUGHTER';
    const OTHERS_RELATIONSHIP = 'OTHERS';

    protected $fillable = [
        'identification_card',
        'name',
        'last_name',
        'email',
        'phone',
        'birthdate',
        'gender'
    ];

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot('relationship');
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    public function getFullNameAttribute()
    {
        return "$this->name $this->last_name";
    }

    public function getRelationshipAttribute()
    {
        return $this->pivot?->relationship;
    }

    public function getMainStreetAttribute()
    {
        return $this->address->main_street;
    }

    public function getSecondaryStreetAttribute()
    {
        return $this->address->secondary_street;
    }

    public function getProvinceAttribute()
    {
        return $this->address->province;
    }

    public function getCityAttribute()
    {
        return $this->address->city;
    }
}
