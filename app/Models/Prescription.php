<?php

namespace App\Models;

use App\Traits\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prescription extends Model
{
    use HasFactory, Filterable;

    protected $fillable = [
        'drug', 'medication'
    ];

    public function appointment()
    {
        return $this->belongsTo(Appointment::class);
    }
}
