<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Speciality extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    public function scopeFilterByName($query, $keyword)
    {
        return $keyword ? $query->where('name', 'LIKE', "%$keyword%") : $query;
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
