<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\HasRole;
use App\Traits\Filterable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable, HasRole, SoftDeletes, Filterable;

    const FEMALE_GENDER = 'F';
    const MALE_GENDER = 'M';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'identification_card',
        'name',
        'last_name',
        'username',
        'email',
        'password',
        'phone',
        'birthdate',
        'gender'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function patients()
    {
        return $this->belongsToMany(Patient::class)->withPivot('relationship');
    }

    public function specialities()
    {
        return $this->belongsToMany(Speciality::class);
    }

    public function exams()
    {
        return $this->hasMany(Exam::class, 'technician_id');
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function getFullNameAttribute()
    {
        return "$this->name $this->last_name";
    }

    public function getRelationshipAttribute()
    {
        return $this->pivot->relationship;
    }
}
