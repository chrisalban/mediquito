<?php

namespace App\Policies;

use App\Models\Prescription;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PrescriptionPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return $user->is_client || $user->is_doctor;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->is_doctor;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Prescription  $prescription
     * @return mixed
     */
    public function update(User $user, Prescription $prescription)
    {
        return $user->is_doctor && $prescription->appointment->doctor->id === $user->id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Prescription  $prescription
     * @return mixed
     */
    public function delete(User $user, Prescription $prescription)
    {
        return $user->is_doctor && $prescription->appointment->doctor->id === $user->id;
    }
}
