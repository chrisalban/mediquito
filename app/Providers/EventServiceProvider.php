<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

use App\Events\AppointmentScheduled;
use App\Listeners\AppointmentScheduleNofification;

use App\Events\AppointmentRescheduled;
use App\Listeners\AppointmentRescheduleNotification;

use App\Events\AppointmentCancelled;
use App\Listeners\AppointmentCancelledNotification;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        AppointmentScheduled::class => [
            AppointmentScheduleNofification::class,
        ],
        AppointmentRescheduled::class => [
            AppointmentRescheduleNotification::class,
        ],
        AppointmentCancelled::class => [
            AppointmentCancelledNotification::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
