<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\User;

class UserIsSoftDeleted implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(
        protected $user = null
    ) {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->user = User::withTrashed()->firstWhere('identification_card', $value); //Buscar el usuario por el número de cédula a pesar de estar eliminado
        return !isset($this->user); //Si el usuario existe devuelve false caso contrario devuelve true
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->user->trashed() ? 'User is soft deleted' : trans('validation.custom.identification_card.unique');
    }
}
