<?php
namespace App\Services;

use App\Models\Appointment;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

/*
* Servicio para las citas
*/
class AppointmentService
{
    public function __construct(
        public Appointment $appointment
    ) {}

    public function scheduleAppointment(array $fields, int $patient_id, int $speciality_id, int $doctor_id, int $previous_id = null) 
    {
        if(!$this->checkOwners($patient_id, $speciality_id, $doctor_id, $previous_id)) abort(422, 'Unprosesable entry');
        
        if($previous_id) {
            // Si se especifica una cita previa se crea una nueva cita con los datos del anterior y la anterior se cierra
            $previous = Appointment::find($previous_id);

            $this->appointment = $previous->replicate()->fill([
                'start_time' => $fields['start_time'],
                'end_time' => $fields['end_time']
            ]);

            $this->appointment->previous()->associate($previous);

            $previous->update([
                'closed_at' => now()
            ]);
        } else {
            // Si no se especifica ninguna cita previa se crea una nueva cita
            $this->appointment = new Appointment($fields);
        }

        $this->setRelationships($patient_id, $doctor_id, $speciality_id)->save();

        return $this;
    }

    public function rescheduleAppointment(Appointment $appointment, array $fields, int $patient_id, int $speciality_id, int $doctor_id )
    {
        if(!$this->checkOwners($patient_id, $speciality_id, $doctor_id)) abort(422, 'Unprosesable entry');

        $this->appointment = $appointment;
        $this->appointment->fill($fields);
        $this->setRelationships($patient_id, $doctor_id, $speciality_id)->save();

        return $this;
    }

    public function updateDescription(Appointment $appointment, string $description)
    {
        $this->appointment = $appointment;
        $this->appointment->update([ 'description' => $description ]);

        return $this;
    }

    public function cancellAppointment(Appointment $appointment)
    {
        $this->appointment = $appointment;
        $this->appointment->update([
            'is_cancelled' => true
        ]);

        return $this;
    }

    private function setRelationships(int $patient_id, int $doctor_id, int $speciality_id)
    {
        $this->appointment->patient()->associate($patient_id);
        $this->appointment->doctor()->associate($doctor_id);
        $this->appointment->speciality()->associate($speciality_id);

        return $this->appointment;
    }

    private function checkOwners($patient, $speciality, $doctor, $previous = null)
    {
        $current_user = Auth::user();

        // Verificar que la especialidad corresponda a una que pertenezca al doctor 
        $doctor = User::with('specialities')->find($doctor);
        if(!$doctor->specialities->contains('id', $speciality)) return false;
        
        // Verificar si el usuario es un cliente que el paciente sea un dependiente
        if($current_user->is_client) {
            $current_user->load('patients');
            if(!$current_user->patients->contains('id', $patient)) return false; 
        }
        
        // Verificar que la cita contenga los mismos datos de la cita anterior
        if($previous) {
            $previous = Appointment::with('speciality:id', 'doctor:id', 'patient:id')->find($previous);
            return match(true) {
                $previous->speciality->id !== $speciality   => false,
                $previous->doctor->id !== $doctor->id       => false,
                $previous->patient->id !== $patient         => false,
                default                                     => true
            };
        }

        return true;
    }

    public function closeAppointment(Appointment $appointment)
    {
        $appointment->update([
            'closed_at' => now()
        ]);
        
        return $this;
    }
}
