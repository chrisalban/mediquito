<?php

namespace App\Services;

class ProvinceService
{

    protected $provinces;

    public function __construct()
    {
        $file_content = file_get_contents(__DIR__.'/../../resources/lang/provincias.json');
        $this->provinces = json_decode($file_content, true);
    }

    public function getProvincesNames()
    {
        $provinces = [];
        foreach ($this->provinces as $province) {
            if (array_key_exists('provincia', $province)) {
                $provinces[] = $province['provincia'];
            }
        }
        return $provinces;
    }

    public function getCitiesNameFromProvinces(String $province_name)
    {
        $cities = [];
        foreach ($this->provinces as $province) {
            if (array_key_exists('provincia', $province)) {
                if ($province['provincia'] === $province_name) {
                    foreach ($province['cantones'] as $city) {
                        if (array_key_exists('canton', $city)) {
                            $cities[] = $city['canton'];
                        }
                    }
                }
            }
        }
        return $cities;
    }
}
