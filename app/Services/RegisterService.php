<?php
namespace App\Services;

use App\Models\User;
use App\Models\Patient;
use App\Models\Address;
use App\Traits\AuthTrait;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Auth\Events\Registered;

/**
 * Servicio para el registro de usuarios y pacientes
 */
class RegisterService
{
    use AuthTrait;

    public function __construct(
        protected Request $request,
        protected Collection $fields,
        protected Address $address,
        protected Patient $patient,
        protected User $user
    ) {}

    /*
     * Método que crea la dirección que compartiran el paciente y el usuario
     */
    public function createAddress(array $fields = null)
    {
        $this->setFields($fields); 

        $this->address = Address::create($this->fields->all());

        return $this;
    }

    /*
    * Metodo que registra un usuario
    */
    public function registerUserAndAttachToPatient(array $fields = null)
    {
        $this->setFields($fields);

        $this->user = User::make($this->fields->all());
        $this->user->forceFill([
            'password' => Hash::make($this->fields->get('password'))
        ]);

        $this->user->assignRole('client');

        $patient = Patient::where([
            [ 'identification_card', $this->fields->get('identification_card') ],
            [ 'birthdate', $this->fields->get('birthdate') ],
            [ 'gender', $this->fields->get('gender') ]
        ])->firstOrFail();

        $this->user->address()->associate($this->address)->save();

        $this->user->patients()->attach($patient, ['relationship' => Patient::HEADLINE_RELATIONSHIP]);

        event(new Registered($this->user));

        return $this;
    }

    /*
    * Metodo que registra un paciente
    */
    public function registerPatientIfNotExist(array $fields = null)
    {
        $this->setFields($fields);

        // Se consulta si el paciente ya existe por el número de cédula

        $patient = Patient::firstWhere('identification_card', $this->fields->get('identification_card'));

        $address_data = $this->fields->only([ 'main_street', 'secondary_street', 'province', 'city' ])->all();
        
        if (isset($patient)) {
            $this->patient = $patient;
            // Si el paciente existen se actualizan los datos ingresados por el usuario
            $this->patient->update($this->fields->only([
                'name',
                'last_name',
                'email',
                'phone'
            ])->all());
            $this->patient->address->update($address_data);
        } else {
            // Si el paciente no existen se crea un nuevo paciente
            $this->patient = Patient::make($this->fields->only([
                'identification_card',
                'name',
                'last_name',
                'email',
                'phone',
                'birthdate',
                'gender'
            ])->all());
            $this->patient->address()->associate($this->address)->save();
        }

        return $this;
    }

    /*
    * Metodo que registra la sesión del usuario creado
    */
    public function loginUserAfterRegister()
    {
        $this->login($this->fields->only('username', 'password')->all(), false, $this->request);
        return $this;
    }

    /*
    * Metodo que establece la propiedad de fields
    */
    private function setFields(array $fields = null)
    {
        if ($fields) $this->fields = collect($fields);
    }
}
