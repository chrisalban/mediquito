<?php

namespace App\Services;

use App\Models\Speciality;

/**
 *  Servicio para la clase Speciality
 */
class SpecialityService
{
    /*
    * Metodo que elimina una especialidad, primero se valida que no tenga usuarios atados
    */
    public function deleteSpecialityiIfDoesntHaveUsers(Speciality $speciality)
    {
        $this->validateDoesntHaveUsers($speciality);
        return $speciality->delete();
    }

    /*
    * Metodo que valida que la especialidad no tenga usuarios atados
    */
    private function validateDoesntHaveUsers(Speciality $speciality)
    {
        if ($speciality->users()->count()) return abort(422, 'cannot delete speciality');
    }
}
