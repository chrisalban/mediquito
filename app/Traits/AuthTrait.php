<?php

namespace App\Traits;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

/**
 * Trait que permite iniciar o cerrar sesión
 */
trait AuthTrait
{
    /*
    * Metodo que inicia sesión
    */
    public function login(array $credentials, Request $request, bool $remember = false )
    {
        if (Auth::attempt($credentials, $remember)) {
            $request->session()->regenerate();

            return redirect()->intended('/');
        }
        return null;
    }

    /*
    * Metodo que cierra sesión
    */
    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();
    }
}
