<?php

namespace Database\Factories;

use App\Models\Appointment;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class AppointmentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Appointment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $time_start = now();
        $time_end = new Carbon($time_start);
        $time_end->addMinutes(30);
        
        return [
            'start_time'    => $time_start,
            'end_time'      => $time_end,
            'description'   => $this->faker->sentence(),
        ];
    }
}
