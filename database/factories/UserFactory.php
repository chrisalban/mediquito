<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->name;
        $last_name = $this->faker->lastName;
        return [
            'identification_card'   => $this->faker->unique()->regexify('^[0-2]{1}[1-9]{1}[0-5]{1}[0-9]{7}$'),
            'name'                  => $name,
            'last_name'             => $last_name,
            'username'              => strtolower($name) . strtolower($last_name),
            'email'                 => $this->faker->unique()->safeEmail,
            // 'email_verified_at'     => now(),->unique()
            'password'              => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'phone'                 => $this->faker->regexify('^09[0-9]{8}'),
            'birthdate'             => $this->faker->date(),
            'gender'                => $this->faker->randomElement([ User::FEMALE_GENDER, User::MALE_GENDER ])
            // 'remember_token'        => Str::random(10),
        ];
    }
}
