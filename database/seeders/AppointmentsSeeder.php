<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Appointment;
use App\Models\Exam;
use App\Models\User;
use App\Models\Patient;
use App\Models\Prescription;
use App\Models\File;
use Illuminate\Support\Collection;

class AppointmentsSeeder extends Seeder
{
    public function __construct(
        protected Collection $doctors
    ) {
        $this->doctors = User::whereHasRole('doctor')->get();
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Citas para el titular
        $client_patient = Patient::whereHas('users', fn ($users) => $users->where('username', 'client'))->first();
        Appointment::factory()->times(10)->make()->each(function ($appointment) use ($client_patient) {
            $this->relationships($appointment, $client_patient);
        });

        // Citas para los dependientes
        $patients = Patient::get();
        Appointment::factory()->times(10)->make()->each(function ($appointment) use ($patients) {
            $this->relationships($appointment, $patients->random());
        });
    }

    private function relationships(Appointment $appointment, $patient)
    {
        $doctor = $this->doctors->random();
        $appointment->patient()->associate($patient);
        $appointment->doctor()->associate($doctor);
        $appointment->speciality()->associate($doctor->specialities->random());
        $appointment->save();
        $appointment->prescriptions()->saveMany(Prescription::factory()->count(3)->make()->all());

        $appointment->exams()->saveMany(Exam::factory()->count(3)->make()->all());

        $appointment->exams->each(function($exam)
        {
            $files = File::factory()->count(3)->make();
            $files->each(fn($file) => $file->technician()->associate($this->doctors->random()));
            $exam->files()->saveMany($files);
        });
    }
}
