import {
    library
} from '@fortawesome/fontawesome-svg-core'
import {
    FontAwesomeIcon
} from '@fortawesome/vue-fontawesome'

import {
    faDoorOpen,
    faAddressCard,
    faUser,
    faUserCircle,
    faKey,
    faHouseUser,
    faArrowCircleLeft,
    faCalendarPlus,
    faUserMd,
    faUserInjured,
    faStethoscope,
    faNotesMedical,
    faEdit,
    faTrash,
    faPlus,
    faChevronLeft,
    faChevronRight,
    faCalendarAlt,
    faCalendarTimes,
    faFileUpload,
    faClipboardCheck,
    faFile,
    faFileMedical,
    faFileMedicalAlt,
    faSyringe,
    faBars,
} from '@fortawesome/free-solid-svg-icons'

library.add(
    faDoorOpen,
    faAddressCard,
    faUser,
    faUserCircle,
    faKey,
    faHouseUser,
    faArrowCircleLeft,
    faCalendarPlus,
    faUserMd,
    faUserInjured,
    faStethoscope,
    faNotesMedical,
    faEdit,
    faTrash,
    faPlus,
    faChevronLeft,
    faChevronRight,
    faCalendarAlt,
    faCalendarTimes,
    faFileUpload,
    faClipboardCheck,
    faFile,
    faFileMedical,
    faFileMedicalAlt,
    faSyringe,
    faBars,
)

export default FontAwesomeIcon
