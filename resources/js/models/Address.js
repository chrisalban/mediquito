/*
* Clase que permite la conección con la api de datos de las especialidades.
*/
import Structure from '../services/Orm'

export default new Structure.Factory({
    attribute: 'address',
    fillable: [
        'mainStreet',
        'secondaryStreet',
        'province',
        'city',
    ]
})
