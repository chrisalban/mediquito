/*
* Clase que permite la conección con la api de datos de los exámenes.
*/
import Structure from '../services/Orm'
import User from './User'
import File from './File'
import AppointmentList from './AppointmentList'

export default new Structure.Factory({
    attribute: 'exams',
    fillable: [
        'name',
        'description',
        'status'
    ],
    relationships: [
        {
            attribute: 'appointment',
            type: 'hasOne',
            model: AppointmentList
        },
        {
            attribute: 'technician',
            type: 'hasOne',
            model: User
        },
        {
            attribute: 'files',
            type: 'hasMany',
            model: File
        }
    ],
    methods: {
        uploadFile(file) {
            return new Promise( (resolve, reject) => {
                let formData = new FormData()
                formData.append('file', file)

                axios.post(`/api/exams/${this.id}/upload_file`, formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                })
                .then( response => resolve(response))
                .catch( errors => reject(errors) )
            })
        },
        deleteFile(file) {
            return new Promise( (resolve, reject) => {
                axios.delete(`/api/exams/${this.id}/delete_file/${file.id}`)
                .then( response => resolve(response))
                .catch( errors => reject(errors) )
            })
        },
        process() {
            return new Promise( (resolve, reject) => {
                axios.put(`/api/exams/${this.id}/process`)
                .then( response => resolve(response) )
                .catch( errors => reject(errors) )
            })
        },
        getReport(params) {
            return new Promise( (resolve, reject) => {
                axios.get('/api/exams/report', { params })
                .then(response => resolve(response))
                .catch( errors => reject(errors) )
            })
        },
    }
})
