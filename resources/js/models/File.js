/*
* Clase que permite la conección con la api de datos de los archivos.
*/
import Structure from '../services/Orm'
import Exam from './Exam'

export default new Structure.Factory({
    attribute: 'files',
    fillable: [
        'url',
        'name'
    ],
    relationships: [
        {
            attribute: 'exam',
            type: 'hasOne',
            model: Exam
        },
    ],
})
