import provinces from '../../lang/provincias'
export default class Province {
    constructor() {
        this.provinces = provinces
    }

    static getProvincesName() {
        let provincesNames = []
        for (var province in provinces) {
            if (provinces[province].provincia) {
                provincesNames.push(this.capitalizeFirstLetter(provinces[province].provincia))
            }
        }
        return provincesNames
    }

    static getCitiesNameFromProvince(provinceName) {
        if (!provinceName) return []
        let citiesNames = []
        for (var province in provinces) {
            if (provinces[province].provincia === provinceName.toUpperCase()) {
                let cantons = provinces[province].cantones
                for (var city in cantons) {
                    citiesNames.push(this.capitalizeFirstLetter(cantons[city].canton))
                }
            }
        }
        return citiesNames
    }

    static capitalizeFirstLetter(string) {
        let lowerString = string.toLowerCase()
        return lowerString.toLowerCase().charAt(0).toUpperCase() + lowerString.slice(1);
    }
}
