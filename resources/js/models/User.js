/*
* Clase que permite la conección con la api de datos de los usuarios.
*/
import Structure from '../services/Orm'
import Speciality from './Speciality'
import Role from './Role'
import Address from './Address'

export default new Structure.Factory({
    attribute: 'user',
    fillable: [
        'identificationCard',
        'name',
        'lastName',
        'username',
        'email',
        'password',
        'phone',
        'birthdate',
        'gender'
    ],
    computed: {
        fullName() {
            return `${this.name} ${this.lastName}`
        },
        relationshipNames() {
            if(!this.role) return
            let relationships

            switch (this.role.name) {
                case 'doctor':
                    relationships = this.specialities
                    break
                case 'admin':
                    relationships = [{ name: 'Administrador'}]
                    break
                default:
                    relationships = [{ name: 'Super' }]
            }
            return relationships
        }
    },
    relationships: [
        {
            attribute: 'role',
            type: 'hasOne',
            model: Role
        },
        {
            attribute: 'specialities',
            type: 'hasMany',
            model: Speciality
        },
        {
            attribute: 'address',
            type: 'hasOne',
            model: Address
        }
    ],
    methods: {
        hasRole(role) {
            if(!this.role) return false
            return this.role.name === role
        },
        doesntHaveRole(role) {
            return !this.hasRole(role)
        },
        hasAnyRole(...role) {
            return role.includes(this.role.name)
        }
    }
})
