/*
 * Vue Router configuration
 */

import Vue from "vue";
import VueRouter from "vue-router";
import middlewares from "./middlewares";

Vue.use(VueRouter);

const router = new VueRouter({
    mode: "history",
    routes: [
        {
            path: "/auth",
            component: require("./components/layouts/Auth").default,
            children: [
                {
                    path: "/login",
                    component: require("./components/auth/Login").default,
                    name: "login"
                },
                {
                    path: "/register",
                    component: require("./components/auth/Register").default,
                    name: "register"
                },
                {
                    path: "/forgot-password",
                    component: require("./components/auth/ForgotPassword")
                        .default,
                    name: "forgot-password"
                },
                {
                    path: "/reset-password/:token",
                    component: require("./components/auth/ResetPassword")
                        .default,
                    name: "reset-password"
                }
            ]
        },
        {
            path: "/",
            component: require("./components/layouts/Main").default,
            redirect: { name: "home" },
            children: [
                {
                    path: "/home",
                    component: require("./components/home/Home").default,
                    name: "home"
                },
                {
                    path: "/users",
                    component: require("./components/layouts/Content").default,
                    children: [
                        {
                            path: "/",
                            component: require("./components/layouts/Index")
                                .default,
                            children: [
                                {
                                    path: "/",
                                    name: "users.index",
                                    beforeEnter: middlewares.middleware(
                                        "role:admin,super"
                                    ),
                                    components: {
                                        default: require("./components/users/index/UserCreateButton")
                                            .default,
                                        list: require("./components/users/index/UserList")
                                            .default
                                    }
                                }
                            ]
                        },
                        {
                            path: "create",
                            name: "users.create",
                            beforeEnter: middlewares.middleware(
                                "role:admin,super"
                            ),
                            component: require("./components/users/create/UserCreate")
                                .default
                        },
                        {
                            path: "edit/:user",
                            name: "users.edit",
                            beforeEnter: middlewares.middleware(
                                "role:admin,super"
                            ),
                            component: require("./components/users/edit/UserEdit")
                                .default
                        }
                    ]
                },
                {
                    path: "/appointments",
                    component: require("./components/layouts/Content").default,
                    children: [
                        {
                            path: "/",
                            component: require("./components/layouts/Index")
                                .default,
                            children: [
                                {
                                    path: "/",
                                    name: "appointments.index",
                                    beforeEnter: middlewares.middleware(
                                        "role:admin,super,client,doctor"
                                    ),
                                    components: {
                                        default: require("./components/appointments/index/AppointmentActions")
                                            .default,
                                        list: require("./components/appointments/index/AppointmentList")
                                            .default
                                    }
                                }
                            ]
                        },
                        {
                            path: 'create',
                            name: 'appointments.create',
                            beforeEnter: middlewares.middleware('role:admin,super,doctor,client'),
                            component: require('./components/appointments/create/AppointmentCreate').default,
                        },
                        {
                            path: 'edit/:appointment',
                            name: 'appointments.edit',
                            beforeEnter: middlewares.middleware('role:admin,super,doctor,client|has-acces-to-appointment'),
                            component: require('./components/appointments/edit/AppointmentEdit').default
                        },
                        {
                            path: 'show/:appointment',
                            name: 'appointments.show',
                            beforeEnter: middlewares.middleware('role:admin,super,doctor,client|has-acces-to-appointment'),
                            component: require('./components/appointments/show/AppointmentShow').default
                        }
                    ]
                },
                {
                    path: '/exams',
                    component: require('./components/exams/index/ExamList').default,
                    name: 'exams.index',
                    beforeEnter: middlewares.middleware('role:super,doctor')
                },
                {
                    path: "/patients",
                    component: require("./components/layouts/Content").default,
                    children: [
                        {
                            path: "/",
                            component: require("./components/layouts/Index")
                                .default,
                            children: [
                                {
                                    path: "/",
                                    name: "patients.index",
                                    beforeEnter: middlewares.middleware(
                                        "role:admin,super,client"
                                    ),
                                    components: {
                                        default: require("./components/patients/index/PatientCreateButton")
                                            .default,
                                        list: require("./components/patients/index/PatientList")
                                            .default
                                    }
                                }
                            ]
                        },
                        {
                            path: "create",
                            name: "patients.create",
                            beforeEnter: middlewares.middleware(
                                "role:admin,super,client"
                            ),
                            component: require("./components/patients/create/PatientCreate")
                                .default
                        },
                        {
                            path: "edit/:patient",
                            name: "patients.edit",
                            beforeEnter: middlewares.middleware(
                                "role:admin,super,client|has-acces-to-patient"
                            ),
                            component: require("./components/patients/edit/PatientEdit")
                                .default
                        }
                    ]
                },
                {
                    path: "/specialities",
                    component: require("./components/layouts/Index").default,
                    children: [
                        {
                            path: "/",
                            name: "specialities.index",
                            beforeEnter: middlewares.middleware(
                                "role:admin,super"
                            ),
                            components: {
                                default: require("./components/specialities/index/SpecialityCreate")
                                    .default,
                                list: require("./components/specialities/index/SpecialityList")
                                    .default
                            }
                        }
                    ]
                },
                {
                    path: '/reports',
                    component: require("./components/layouts/Content").default,
                    children: [
                        {
                            path: 'history',
                            component: require('./components/reports/history/HistoryList').default,
                            name: 'reports.history.index',
                            beforeEnter: middlewares.middleware('role:super,doctor,client,admin')
                        },
                        {
                            path: 'exams',
                            component: require('./components/reports/exams/ExamList').default,
                            name: 'reports.exams.index',
                            beforeEnter: middlewares.middleware('role:super,doctor,client,admin')
                        },
                        {
                            path: 'prescriptions',
                            component: require('./components/reports/prescriptions/PrescriptionList').default,
                            name: 'reports.prescriptions.index',
                            beforeEnter: middlewares.middleware('role:super,doctor,client,admin')
                        },
                    ]
                },
                {
                    path: "/unauthorized",
                    component: require("./components/partials/403").default,
                    name: "403"
                },
                {
                    path: "/email/verify",
                    component: require("./components/partials/verifyEmail")
                        .default,
                    name: "verification.notice"
                },
                {
                    path: "/error",
                    component: require("./components/partials/500").default,
                    name: "500"
                },
                {
                    path: "/*",
                    component: require("./components/partials/404").default,
                    name: "404"
                }
            ]
        }
    ]
});

router.beforeEach(middlewares.middleware("auth"));

export default router;
