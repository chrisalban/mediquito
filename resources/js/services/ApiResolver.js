/*
* Clase que permite resolver las rutas del api
*/
const pluralize = require('pluralize')

export default class ApiResolver {
    static resolveIndex(className) {
        return `/api/${pluralize(className.toLowerCase())}`
    }

    static resolveShow(className, object) {
        return `/api/${pluralize(className.toLowerCase())}/${object.id}`
    }

    static resolveStore(className) {
        return `/api/${pluralize(className.toLowerCase())}`
    }

    static resolveUpdate(className, object) {
        return `/api/${pluralize(className.toLowerCase())}/${object.id}`
    }

    static resolveDestroy(className, object) {
        return `/api/${pluralize(className.toLowerCase())}/${object.id}`
    }

    static resolveRestore(className) {
        return `/api/${pluralize(className.toLowerCase())}/restore`
    }

    static camelize(field) {
        return field.replace(/_[A-Za-z]/g, word => word[1].toUpperCase())
    }

    static underscore(field) {
        return field.replace(/[A-Z]/g, word => `_${word.toLowerCase()}`)
    }

    static resolveRegister() {
        return '/register'
    }

    static underscoreParams(params) {
        let underscoredParams = {}
        for (let param in params) {
            if (typeof param === 'Object') underscoredParams[ApiResolver.underscore(param)] = this.underscoredParams(param)
            else underscoredParams[ApiResolver.underscore(param)] = params[param]
        }
        return underscoredParams
    }

    static camelizeParams(params) {
        let camelisedParams = {}
        for (let param in params) {
            if (typeof param === 'Object') camelisedParams[ApiResolver.camelize(param)] = this.camelizeParams(param)
            else camelisedParams[ApiResolver.camelize(param)] = params[param]
        }
        return camelisedParams
    }
}
