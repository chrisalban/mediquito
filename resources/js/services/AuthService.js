/*
* Clase que permite manejar la autenticación del sistema
*/
import ApiResolver from './ApiResolver'

export default class AuthService {
    static register(form) {
        return new Promise((resolve, reject) => {
            axios.post(ApiResolver.resolveRegister(), ApiResolver.underscoreParams(form))
            .then(response => resolve(response))
            .catch(errors => {
                errors.response.data.errors = ApiResolver.camelizeParams(errors.response.data.errors)
                reject(errors)
            })
        })
    }
}
