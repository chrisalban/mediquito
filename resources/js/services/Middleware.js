export default class RouterMiddleware {
    constructor(
        middlewares = []
    ) {
        this.middlewares = []
        this.registerMiddlewares(middlewares)
    }

    middleware(names) {
        let aliases = names.split('|')

        return async (to, from, next) => {
            for (let alias of aliases) {
                let response = await this.callMiddleware(this.resolveMiddleware(alias), this.resolveParams(alias), to, from)

                if(response) next(response)
            }
            next()
        }
    }

    resolveMiddleware(param) {
        let middlewareObj = param.split(':')
        let name = middlewareObj[0]
        return this.middlewares.find(middleware => middleware.name === name)
    }

    resolveParams(param) {
        let middlewareObj = param.split(':')
        let params = middlewareObj[1] ? middlewareObj[1].split(',') : null

        return params
    }

    callMiddleware(middleware, params, to, from) {
        return new Promise(resolve => {
            middleware.handler(to, from, resolve, params)
        })
    }

    registerMiddlewares(middlewares) {
        middlewares.forEach(middleware => {
            this.middlewares.push({
                name:       middleware.name,
                handler:    middleware.handler
            })
        })
    }
}
