/*
* Clase que permite la conección con la api del sistema.
*/
import ApiResolver from '../services/ApiResolver'

export class Model {

    /*
    * Metodo que hace una llamada GET al methodo para listar los objectos del api
    */
    all(params = {}) {
        return new Promise( (resolve, reject) => {
            axios.get(ApiResolver.resolveIndex(this.attribute), { params: ApiResolver.underscoreParams(params) })
            .then( response => resolve(response))
            .catch(errors => reject(errors.response))
        })
    }

    /*
    * Metodo para buscar un objeto por su id
    */
    findById(id) {
        return new Promise( (resolve, reject) => {
            axios.get(ApiResolver.resolveShow(this.attribute, { id }))
            .then( response => resolve(response))
            .catch( errors => reject(errors.response))
        })
    }

    /*
    * Metodo que hace una llamada POST al methodo para guardar objectos del api
    */
    save(params = null) {
        return new Promise( (resolve, reject) => {
            let fillableParams = this.fillableParams
            delete fillableParams.id

            if(params) fillableParams = {  ...fillableParams, ...ApiResolver.underscoreParams(params) }

            axios.post( ApiResolver.resolveStore(this.attribute), fillableParams )
            .then( response => resolve(response))
            .catch( errors => {
                errors.response.data.errors = ApiResolver.camelizeParams(errors.response.data.errors)
                reject(errors.response)
            })
        })
    }

    /*
    * Metodo que hace una llamada PUT al methodo para actualizar objectos del api
    *
    */
    update(params = null) {
        return new Promise( (resolve, reject) => {
            let fillableParams = this.fillableParams
            if(params) fillableParams = {  ...this.fillableParams, ...ApiResolver.underscoreParams(params) }
            axios.put(ApiResolver.resolveUpdate(this.attribute, this), fillableParams)
            .then( response => resolve(response))
            .catch( errors => reject(errors.response))
        })
    }

    /*
    * Metodo que hace una llamada DELETE al methodo para eliminar objectos del api
    */
    destroy() {
        return new Promise( (resolve, reject) => {
            axios.delete(ApiResolver.resolveDestroy(this.attribute, this), this.fillableParams)
            .then( response => resolve(response))
            .catch( errors => reject(errors.response))
        })
    }

    /*
    * Metodo que hace una llamada PUT al metodo para restaurar un objeto
    */
    restore(field = { fieldName: 'id'}) {
        return new Promise( (resolve, reject) => {
            let obj = {}
            obj[field.fieldName] = this[field.fieldName]

            axios.put(ApiResolver.resolveRestore(this.attribute, this), ApiResolver.underscoreParams(obj))
            .then( response => resolve(response))
            .catch( errors => reject(errors.response))
        })
    }

    /*
    * Mapeo de campos a atributos
    */
    mapFields(fields = []) {
        this.fields = fields
        fields.forEach(field => this[ApiResolver.camelize(field)] = null)
    }

    /*
    * Mapeo de las relaciones a atributos
    */
    mapRelationships(relationships = []) {
        this.relationships = relationships
        //Se recorren las relaciones especificadas
        relationships.forEach(relationship => {
            //Se obtiene el metodo de relación de los metodos hasOne o hasMany y el nombre del atributo
            let model = relationship.model
            let attribute = `get${relationship.attribute.charAt(0).toUpperCase() + relationship.attribute.slice(1)}Attribute`

            //Se define un getter para acceder a la propiedad 'getNombreRelacionAttribute'
            this.__defineGetter__(relationship.attribute, () => this[attribute])
            //Se define un setter para establecer a la propiedad 'getNombreRelacionAttribute'
            this.__defineSetter__(relationship.attribute, value => this[attribute] = relationship.type === 'hasOne' ? model.make(value) : model.collection(value))
        })
    }

    /*
    * Metodo que mapea los metodos del objeto
    */
    mapMethods(methods = {}) {
        this.methods = methods
        for (let method in methods) {
            this[method] = methods[method]
        }
    }

    /*
    * Metodo que mapea las propiedades coputadas
    */

    mapComputed(computed = {}) {
        this.computed = computed
        for (let property in computed) {
            this.__defineGetter__(property, computed[property])
        }
    }

    /*
    * Metodo que llena los atributos del objeto con los que se pasan en el constructor
    */
    fill(fields) {
        if (!fields || fields === []) return
        // Se crea un objeto genérico con los datos enviados por la api
        let model = {}
        for (let field in fields) {
            model[ApiResolver.camelize(field)] = fields[field]
        }
        //Se rellenan los atributos del objeto actual con los del objeto genérico
        this.fillable.forEach(field => {
            if(model[field])
                this[field] = model[field]
        })
        //Se crean y se asignan las relaciónes del modelo
        this.relationships.forEach(relationship => {
            if(model[relationship.attribute])
                this[relationship.attribute] = model[relationship.attribute]
        })
    }

    /*
    * Methodo que convierte los atributos del objeto a un objeto que entienda el api
    */
    get fillableParams() {
        let fields = {}

        this.relationships.forEach(relationship => {
            if (this[relationship.attribute]) {
                if (relationship.type === 'hasOne') {
                    fields[relationship.attribute] = this[relationship.attribute].fillableParams
                } else {
                    fields[relationship.attribute] = []
                    this[relationship.attribute].forEach(obj => {
                        fields[relationship.attribute].push(obj.fillableParams)
                    })
                }
            }
        })

        fields = { ...fields, ...this.fillable.reduce( (acumulator, field) => {
            if (this[field]) acumulator[ApiResolver.underscore(field)] = this[field]
            return acumulator
        }, {})}

        return fields
    }

    get params() {
        return ApiResolver.camelizeParams(this.fillableParams)
    }
}

class Factory {
    constructor (structure) {
        this.structure = structure
    }

    /*
    * Metodo que consulta todos los objetos
    */
    all(params = {}) {
        const model = this.make()
        return new Promise( (resolve, reject) => {
            model.all(params)
            .then( response => {
                response.data.data = this.collection(response.data.data)
                resolve(response)
            })
            .catch( errors => {
                reject(errors)
            })
        })
    }

    findById(id) {
        const model = this.make()
        return new Promise( (resolve, reject) => {
            model.findById(id)
            .then( response => {
                response.data.data = this.make(response.data.data)
                resolve(response)
            })
            .catch( errors => {
                reject(errors)
            })
        })
    }

    /*
    * Metodo que genera nuevo Objeto
    */
    make(fields) {
        const model = new Model()

        model.attribute = this.structure.attribute
        model.fillable = ['id', ...this.structure.fillable]

        model.mapFields(this.structure.fillable)
        model.mapComputed(this.structure.computed)
        model.mapRelationships(this.structure.relationships)
        model.mapMethods(this.structure.methods)
        model.fill(fields)

        return model
    }

    /*
    * Metodo que genera una colección de Objetos
    */
    collection(fields) {
        if(!fields) return null
        return fields.map(item => this.make(item))
    }
}

class Structure {
    constructor() {
        this.Factory = Factory
    }
}

export default new Structure()
