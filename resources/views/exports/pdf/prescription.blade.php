<style>
    h5 {
        font-size: 1.25rem;
    }
    .prescriptions, .info {
        width: 100%;
    }
    .prescriptions .drug {
        width: 25%;
    }
    .prescriptions .medication {
        width: 75%;
    }
</style>
<h5>Receta médica</h5>
<table class="info">
    <tbody>
        <tr>
            <td>
                <span class="d-block"><strong>Paciente: </strong>{{ $patient->full_name }}</span>
            </td>
            <td>
                <span class="d-block"><strong>Genero: </strong>{{ $patient->gender === 'M' ? 'Masculino' : 'Femenino'  }}</span>
            </td>
        </tr>
        <tr>
            <td>
                <span class="d-block"><strong>Doctor: </strong>{{ $doctor->full_name }}</span>
            </td>
            <td>
                <span class="d-block"><strong>Teléfono: </strong>{{ $doctor->phone }}</span>
            </td>
        </tr>
    </tbody>
</table>
<h5>Receta</h5>
<table class="prescriptions">
    <thead>
        <tr>
            <th>Medicamento</th>
            <th>Tratamiento</th>
        </tr>
    </thead>
    <tbody>
        @foreach($prescriptions as $prescription)
            <tr>
                <td class="drug">{{ $prescription->drug }}</td>
                <td class="medication">{{ $prescription->medication }}</td>
            </tr>
        @endforeach
    </tbody>
</table> 
<p>Recuerde seguir el tratamiento como se lo indicó el médico</p>
