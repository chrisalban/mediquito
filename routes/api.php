<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\Speciality\SpecialityController;
use App\Http\Controllers\Patient\PatientController;
use App\Http\Controllers\Appointment\AppointmentController;
use App\Http\Controllers\ExamController;
use App\Http\Controllers\Prescription\PrescriptionController;
use App\Http\Resources\AuthUserResource;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware([ 'verified', 'auth:sanctum' ])->get('/user', fn (Request $request) => AuthUserResource::make($request->user()->load('role')));

Route::apiResource('specialities', SpecialityController::class, [ 'except' => [ 'show' ] ]);

Route::put('/users/restore', [UserController::class, 'restore'])->name('users.restore');

Route::put('/appointments/reschedule/{appointment}', [ AppointmentController::class, 'reschedule' ]);
Route::put('/appointments/description/{appointment}', [ AppointmentController::class, 'updateDescription' ]);
Route::put('/appointments/close/{appointment}', [ AppointmentController::class, 'close' ]);
Route::get('/appointments/prescriptions/pdf/{appointment}', [ AppointmentController::class, 'downloadPrescription' ]);

Route::apiResource('appointments', AppointmentController::class, [ 'except' => [ 'update' ] ]);

Route::apiResource('/prescriptions', PrescriptionController::class, [ 'except' => [ 'show' ] ]);

Route::post('/exams/{exam}/upload_file', [ ExamController::class, 'uploadFile' ]);
Route::delete('/exams/{exam}/delete_file/{file}', [ ExamController::class, 'deleteFile' ]);
Route::put('/exams/{exam}/process', [ ExamController::class, 'process' ]);
Route::get('/exams/report', [ ExamController::class, 'getReport' ]);

Route::apiResources([
    'users'         => UserController::class,
    'patients'      => PatientController::class,
    'exams'         => ExamController::class
]);
