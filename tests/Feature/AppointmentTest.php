<?php

namespace Tests\Feature;

use App\Models\Address;
use App\Models\Appointment;
use App\Models\Patient;
use App\Models\Role;
use App\Models\Speciality;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class AppointmentTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_list_appointments()
    {
        $response = $this->get('/api/users');

        $response->assertRedirect('/login');
        
        $user = User::factory()
                    ->for(Address::factory())
                    ->for(Role::factory()->create([ 'name' => 'admin' ]))
                    ->has(Speciality::factory())
                    ->create();

        Sanctum::actingAs($user);

        $address = Address::factory()->create();
        $appointment = Appointment::factory()
                   ->for(User::factory()->for(Role::factory())->for($address), 'doctor')
                   ->for(Patient::factory()->for($address))
                   ->for(Speciality::factory())
                   ->create();
        
        $response = $this->get('/api/appointments');

        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                [
                    'start_time'    => $appointment->start_time,
                    'end_time'      => $appointment->end_time,
                    'description'   => $appointment->description,
                    'is_cancelled'  => $appointment->is_cancelled,
                    'closed_at'     => $appointment->closed_at,
                    'doctor'        => [
                        'name'      => $appointment->doctor->name
                    ],
                    'patient'       => [
                        'name'      => $appointment->patient->name
                    ],
                ]
            ]
        ]);

        $user->role->update([ 'name' => 'doctor' ]);

        $response = $this->get('/api/users');

        $response->assertStatus(200);

        $user->role->update([ 'name' => 'client' ]);

        $response = $this->get('/api/users');

        $response->assertStatus(200);
    }

    public function test_schedule_appointment()
    {
        $address = Address::factory()->create();
        $user = User::factory()
                    ->for(Address::factory())
                    ->for(Role::factory()->create([ 'name' => 'admin' ]))
                    ->has(Patient::factory()->for($address))
                    ->create();

        $doctor = User::factory()
                      ->for($address)
                      ->for(Role::factory())
                      ->has(Speciality::factory())
                      ->create();
        $data = collect([
            'start_time'    => '2022-01-01 12:00:00',
            'end_time'      => '2022-01-01 12:30:00',
            'doctor'        => $doctor->id,
            'speciality'    => $doctor->specialities->first()->id,
            'patient'       => $user->patients->first()->id
        ]);

        $db = $data->only([
            'start_time',
            'end_time'
        ])->all();

        $response = $this->post('/api/appointments', $data->all());

        $response->assertRedirect('/login');

        $this->assertDatabaseMissing('appointments', $db);
        
        Sanctum::actingAs($user);

        $response = $this->post('/api/appointments', $data->all());

        $response->assertStatus(200);

        $this->assertDatabaseHas('appointments', $db);

        $user->role->update([ 'name' => 'doctor' ]);

        $response = $this->post('/api/appointments', $data->all());

        $response->assertSuccessful();

        $user->role->update([ 'name' => 'client' ]);

        $response = $this->post('/api/appointments', $data->all());

        $response->assertSuccessful();
    }

    public function test_reschedule_appointment()
    {
        $address = Address::factory()->create();
        $user = User::factory()
                    ->for(Address::factory())
                    ->for(Role::factory()->create([ 'name' => 'admin' ]))
                    ->has(Patient::factory()->for($address))
                    ->create();

        $doctor = User::factory()
                      ->for($address)
                      ->for(Role::factory())
                      ->has(Speciality::factory())
                      ->create();

        $appointment = Appointment::factory()
                                  ->for($user->patients->first(), 'patient')
                                  ->for($doctor, 'doctor')
                                  ->for(Speciality::factory())
                                  ->create();

        $data = collect([
            'start_time'    => '2022-01-01 12:00:00',
            'end_time'      => '2022-01-01 12:30:00',
            'doctor'        => $doctor->id,
            'speciality'    => $doctor->specialities->first()->id,
            'patient'       => $user->patients->first()->id
        ]);

        $db = $data->only([
            'start_time',
            'end_time'
        ])->all();

        $response = $this->put("/api/appointments/reschedule/$appointment->id", $data->all());

        $response->assertRedirect('/login');

        $this->assertDatabaseMissing('appointments', $db);
        
        Sanctum::actingAs($user);

        $response = $this->put("/api/appointments/reschedule/$appointment->id", $data->all());

        $response->assertStatus(200);

        $this->assertDatabaseHas('appointments', $db);

        $user->role->update([ 'name' => 'doctor' ]);

        $response = $this->put("/api/appointments/reschedule/$appointment->id", $data->all());

        $response->assertForbidden();

        $user->role->update([ 'name' => 'client' ]);

        $response = $this->put("/api/appointments/reschedule/$appointment->id", $data->all());

        $response->assertSuccessful();
    }

    public function test_cancel_appointment()
    {
        $address = Address::factory()->create();
        $user = User::factory()
                    ->for(Address::factory())
                    ->for(Role::factory()->create([ 'name' => 'admin' ]))
                    ->has(Patient::factory()->for($address))
                    ->create();

        $doctor = User::factory()
                      ->for($address)
                      ->for(Role::factory())
                      ->has(Speciality::factory())
                      ->create();

        $appointment = Appointment::factory()
                                  ->for($user->patients->first(), 'patient')
                                  ->for($doctor, 'doctor')
                                  ->for(Speciality::factory())
                                  ->create();

        $response = $this->delete("/api/appointments/$appointment->id");

        $response->assertRedirect('/login');

        $this->assertDatabaseHas('appointments', [
            'id' => $appointment->id
        ]);
        
        $user = User::factory()
                    ->for(Address::factory())
                    ->for(Role::factory()->create([ 'name' => 'admin' ]))
                    ->create();

        Sanctum::actingAs($user);

        $user->role->update([ 'name' => 'doctor' ]);

        $response = $this->delete("/api/appointments/$appointment->id");

        $response->assertForbidden();

        $user->role->update([ 'name' => 'client' ]);

        $response = $this->delete("/api/appointments/$appointment->id");

        $response->assertForbidden();

        $user->role->update([ 'name' => 'admin' ]);

        $response = $this->delete("/api/appointments/$appointment->id");

        $response->assertStatus(200);
        
        $response->assertJson([
            'success' => 'appointment cancelled'
        ]);
    }
}
