<?php

namespace Tests\Feature;

use App\Models\Address;
use App\Models\Role;
use App\Models\Speciality;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_list_users()
    {
        $response = $this->get('/api/users');

        $response->assertRedirect('/login');
        
        $user = User::factory()
                    ->for(Address::factory())
                    ->for(Role::factory()->create([ 'name' => 'admin' ]))
                    ->has(Speciality::factory())
                    ->create();

        Sanctum::actingAs($user);
        
        $response = $this->get('/api/users');

        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                [
                    'name' => $user->name,
                    'last_name' => $user->last_name,
                    'specialities' => [
                        [
                            'id' => $user->specialities->first()->id,
                            'name' => $user->specialities->first()->name
                        ]
                    ],
                    'role' => [
                        'name' => $user->role->name
                    ],
                ]
            ]
        ]);

        $user->role->update([ 'name' => 'doctor' ]);

        $response = $this->get('/api/users');

        $response->assertStatus(200);

        $user->role->update([ 'name' => 'client' ]);

        $response = $this->get('/api/users');

        $response->assertStatus(200);
    }

    public function test_create_users()
    {
        $user = User::factory()
                    ->for(Address::factory())
                    ->for(Role::factory()->create([ 'name' => 'admin' ]))
                    ->create();

        $data = collect([
            'identification_card'   => $this->faker->regexify('^[0-2]{1}[1-9]{1}[0-5]{1}[0-9]{7}$'),
            'name'                  => $this->faker->name,
            'last_name'             => $this->faker->lastName,
            'username'              => $this->faker->word,
            'email'                 => $this->faker->safeEmail,
            'password'              => 'password',
            'password_confirmation' => 'password',
            'role'                  => [
                'name' => Role::factory()->create()->name
            ],
            'phone'                 => $this->faker->regexify('^09[0-9]{8}'),
            'birthdate'             => $this->faker->date(),
            'gender'                => $this->faker->randomElement([ User::FEMALE_GENDER, User::MALE_GENDER ]),
            'specialities'          => [
                [
                    'id' => Speciality::factory()->create()->id,
                ]
            ],
            'address'               => [
                'main_street'       => $this->faker->word,
                'secondary_street'  => $this->faker->word,
                'city'              => $this->faker->word,
                'province'          => $this->faker->word,
            ]
        ]);

        $db = $data->only([
            'identification_card',
            'name',
            'last_name',
            'username',
            'email',
            'phone',
            'birthdate',
            'gender',
        ])->all();

        $response = $this->post('/api/users', $data->all());

        $response->assertRedirect('/login');

        $this->assertDatabaseMissing('users', $db);
        
        Sanctum::actingAs($user);

        $response = $this->post('/api/users', $data->all());

        $response->assertStatus(200);

        $this->assertDatabaseHas('users', $db);

        $user->role->update([ 'name' => 'doctor' ]);

        $response = $this->post('/api/users', $data->all());

        $response->assertForbidden();

        $user->role->update([ 'name' => 'client' ]);

        $response = $this->post('/api/users', $data->all());

        $response->assertForbidden();
    }

    public function test_update_users()
    {
        $update_user = User::factory()
                    ->for(Address::factory())
                    ->for(Role::factory())
                    ->create();
        
        $data = collect([
            'identification_card'   => $this->faker->regexify('^[0-2]{1}[1-9]{1}[0-5]{1}[0-9]{7}$'),
            'name'                  => $this->faker->name,
            'last_name'             => $this->faker->lastName,
            'username'              => $this->faker->word,
            'email'                 => $this->faker->safeEmail,
            'password'              => 'password',
            'password_confirmation' => 'password',
            'role'                  => [
                'name' => Role::factory()->create()->name
            ],
            'phone'                 => $this->faker->regexify('^09[0-9]{8}'),
            'birthdate'             => $this->faker->date(),
            'gender'                => $this->faker->randomElement([ User::FEMALE_GENDER, User::MALE_GENDER ]),
            'specialities'          => [
                [
                    'id' => Speciality::factory()->create()->id,
                ]
            ],
            'address'               => [
                'main_street'       => $this->faker->word,
                'secondary_street'  => $this->faker->word,
                'city'              => $this->faker->word,
                'province'          => $this->faker->word,
            ]
        ]);

        $db = $data->only([
            'identification_card',
            'name',
            'last_name',
            'username',
            'email',
            'phone',
            'birthdate',
            'gender',
        ])->all();

        $response = $this->put("/api/users/$update_user->id", $data->all());

        $response->assertRedirect('/login');

        $this->assertDatabaseMissing('users', $db);
        
        $user = User::factory()
                    ->for(Address::factory())
                    ->for(Role::factory()->create([ 'name' => 'admin' ]))
                    ->create();

        Sanctum::actingAs($user);

        $response = $this->put("/api/users/$update_user->id", $data->all());

        $response->assertStatus(200);

        $this->assertDatabaseHas('users', $db);

        $user->role->update([ 'name' => 'doctor' ]);

        $response = $this->put("/api/users/$update_user->id", $data->all());

        $response->assertForbidden();

        $user->role->update([ 'name' => 'client' ]);

        $response = $this->put("/api/users/$update_user->id", $data->all());

        $response->assertForbidden();
    }

    public function test_delete_users()
    {
        $delete_user = User::factory()
                    ->for(Address::factory())
                    ->for(Role::factory())
                    ->create();

        $response = $this->delete("/api/users/$delete_user->id");

        $response->assertRedirect('/login');

        $this->assertDatabaseHas('users', [
            'id' => $delete_user->id
        ]);
        
        $user = User::factory()
                    ->for(Address::factory())
                    ->for(Role::factory()->create([ 'name' => 'admin' ]))
                    ->create();

        Sanctum::actingAs($user);

        $user->role->update([ 'name' => 'doctor' ]);

        $response = $this->delete("/api/users/$delete_user->id");

        $response->assertForbidden();

        $user->role->update([ 'name' => 'client' ]);

        $response = $this->delete("/api/users/$delete_user->id");

        $response->assertForbidden();

        $user->role->update([ 'name' => 'admin' ]);

        $response = $this->delete("/api/users/$delete_user->id");

        $response->assertStatus(200);
        
        $response->assertJson([
            'success' => 'user deleted'
        ]);
    }
}
