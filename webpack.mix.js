const mix = require('laravel-mix');
const path = require('path')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig({
        resolve: {
            alias: {
                'dayjs': path.resolve(__dirname, 'node_modules', 'dayjs'),
            }
        }
   })
   .js('resources/js/app.js', 'public/js')
   .sass('resources/scss/app.scss', 'public/css');
